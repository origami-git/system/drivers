#pragma once

#include <driver/driver.hpp>

namespace core::driver {

	class ExampleDriver : public Driver {

		ExampleDriver();

		int _load();
		int _unload();
	};
}
